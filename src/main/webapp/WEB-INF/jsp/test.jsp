<%--
 * 
 * @author Shengzhao Li
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>Test</title>
</head>
<body>
<div>
    Now is <strong>${date}</strong>.
    <br/>
    <a href="javascript:history.back();">Back</a>
</div>
</body>
</html>