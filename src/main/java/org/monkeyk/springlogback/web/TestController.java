package org.monkeyk.springlogback.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

/**
 * @author Shengzhao Li
 */
@Controller
public class TestController {

    private static Logger logger = LoggerFactory.getLogger(TestController.class);

    @RequestMapping("test.htm")
    public String test(Model model) {
        logger.info("A visitor is coming...");
        model.addAttribute("date", new Date());
        return "test";
    }

}